import numpy as np
import pickle
import glob
import matplotlib.pyplot as plt
import cv2
import pandas as pd

train_data = []
label_data = []

for x in glob.glob("C:\\Users\\Lenovo\\Desktop\\JAGUNG\\*.jpg"):
	train_data.append((cv2.cvtColor(cv2.imread(x),cv2.COLOR_BGR2RGB)))
	label_data.append(0)

for x in glob.glob("C:\\Users\\Lenovo\\Desktop\\PADI\\*.jpg"):
	train_data.append((cv2.cvtColor(cv2.imread(x),cv2.COLOR_BGR2RGB)))
	label_data.append(1)

for x in glob.glob("C:\\Users\\Lenovo\\Desktop\\KEDELAI\\*.jpg"):
	train_data.append((cv2.cvtColor(cv2.imread(x),cv2.COLOR_BGR2RGB)))
	label_data.append(2)

for x in glob.glob("C:\\Users\\Lenovo\\Desktop\\KACANG\\*.jpg"):
	train_data.append((cv2.cvtColor(cv2.imread(x),cv2.COLOR_BGR2RGB)))
	label_data.append(3)

train_data_BIJI = {"train_data" : np.array(train_data), "label_data" : np.array(label_data)}
print(train_data_BIJI["train_data"].shape)
output = open('C:\\Users\\Lenovo\\Desktop\\DatasetBiji.p','wb')
pickle.dump(train_data_BIJI,output)
output.close()

csvWrite = open('C:\\Users\\Lenovo\\Desktop\\DatasetBiji.csv','w')
columnTitleRow = "ClassID,ClassBiji\n"
csvWrite.write(columnTitleRow)
csvWrite.write("0,JAGUNG\n")
csvWrite.write("1,PADI\n")
csvWrite.write("2,KEDELAI\n")
csvWrite.write("3,KACANG\n")
csvWrite.close()
